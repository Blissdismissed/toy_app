class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  def test
    render html: "testing 1, 2, 3"
  end
  
end
